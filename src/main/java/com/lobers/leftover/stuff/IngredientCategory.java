package com.lobers.leftover.stuff;

public enum IngredientCategory {
    MEAT("meat"),
    FISH("fish"),
    ANIMAL_PRODUCT("a"),
    FRUIT("a"),
    VEGETABLE("a"),
    SPICE("a"),
    GRAIN("a"),
    LEGUME("a");

    private final String name;

    IngredientCategory(String name){
        this.name = name;
    }

    String getName(){
        return this.name;
    }

}
