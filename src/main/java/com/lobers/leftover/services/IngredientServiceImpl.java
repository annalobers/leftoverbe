package com.lobers.leftover.services;

import com.lobers.leftover.DAO.IngredientDAO;
import com.lobers.leftover.entity.Ingredient;

import com.lobers.leftover.services.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class IngredientServiceImpl implements IngredientService {
//    // need to inject customer dao
//    @Autowired
//    private IngredientDAO ingredientDAO;
//
//    @Override
//    @Transactional
//    public void saveIngredient(String ingredient){
//        ingredientDAO.saveIngredient(ingredient);
//    }
//
//    @Override
//    @Transactional
//    public List<Ingredient> getIngredients(){
//        return ingredientDAO.getIngredients();
//    }
//
//    @Override
//    @Transactional
//    public Ingredient getIngredient(int id){
//        return ingredientDAO.getIngredient(id);
//    }
//
//    @Override
//    @Transactional
//    public void deleteIngredient(int id){
//        ingredientDAO.deleteIngredient(id);
//    }
}
