package com.lobers.leftover.controller;

import com.lobers.leftover.entity.Ingredient;
import com.lobers.leftover.stuff.IngredientCategory;
import com.lobers.leftover.stuff.IngredientRepository;
import org.springframework.web.bind.annotation.*;

//@RestController
//@CrossOrigin(origins = "http://localhost:4200")
//@Controller
//@CrossOrigin(origins = "http://localhost:4200")
//@RequestMapping("/ingredient")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class IngredientController {

//    @Autowired
//    private IngredientService ingredientService;

    private final IngredientRepository ingredientRepository;

    public IngredientController(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @PostMapping("/saveIngredient")
    void saveIngredient(@RequestBody String ingredient) {

        Ingredient theIngredient = new Ingredient();
        theIngredient.setIngredientName(ingredient);
        theIngredient.setIngredientCategory(IngredientCategory.MEAT);
        System.out.print(theIngredient.getIngredientCategory());
        ingredientRepository.save(theIngredient);
//        ingredientServicice.saveIngredient(ingredient);
    }
}
